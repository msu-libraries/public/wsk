import codecs

with codecs.open("/Users/higgi135/Projects/wsk/MilitiaNames.csv", "r", "utf-8") as open_file:
    militias = open_file.readlines()


with codecs.open("/Users/higgi135/Projects/wsk/militia_searches.csv", "w") as output_file:
    for m in militias:
        searches = []
        m_terms = [mil.rstrip().replace("(", "").replace(")", "") for mil in m.split(",") if mil.rstrip()]
        for x in range(1, len(m_terms)):
            search = "({0} AND {1})".format(('"'+m_terms[0]+'"').encode("utf-8"), ('"'+m_terms[x]+'"').encode("utf-8"))
            searches.append(search)

        search_query = " OR ".join(searches)
        output_file.write(",".join([m_terms[1].decode("utf-8"), search_query.decode("utf-8")]).encode("utf-8") + "\n")
