from __future__ import division
from lnapi import WskApi
import codecs
import base64
import time
import math
import os

class LnCalls():

    def __init__(self):
        self.home_dir = os.path.dirname(os.path.realpath(__file__))
        self.search_file_dict = []

    def run_searches_from_csv(self, file_path, prepared_search=False):
        total_results = 0
        wsk = WskApi()
        wsk.set_source_information(["8399"])
        wsk.set_retrieval_options()

        self.csv_file = file_path
        with codecs.open("/Users/higgi135/Projects/wsk/militia_search_results.txt", "w", "utf-8") as output_file:
            with codecs.open(self.csv_file, "r", "utf-8") as csv:

                for line in csv:

                    fields = line.split(",")
                    self.search_name = fields[0].rstrip()
                    if prepared_search:
                        self.search_query = fields[1]

                    else:

                        if fields[1].rstrip().count("(") > 1:
                            self.search_query = "("+fields[1].rstrip()+")"+ ' AND (militia OR paramilitary OR "death squad" OR vigilante OR "irregular forces")'

                        else:
                            self.search_query = fields[1].rstrip() + ' AND (militia OR paramilitary OR "death squad" OR vigilante OR "irregular forces")'


                    try:
                        wsk.set_retrieval_options()
                        wsk.search(self.search_query)
                        result_count = wsk.result_count
                        print ",".join([self.search_name, self.search_query, str(result_count)])
                        if int(result_count) > 0:
                            self.save_docs(wsk.search_results)

                            
                            total_results += int(result_count)
                            if result_count > 10:
                                iterations = int(math.ceil(int(result_count) / 10))
                                for i in range(2, iterations + 1, 1):
                                    wsk.set_retrieval_options(range_begin=str(10*i-9), range_end=str(10*i))
                                    wsk.search(self.search_query)
                                    self.save_docs(wsk.search_results)

                    except Exception as e:

                        print e
                        result_count = "ERROR"
                        print ",".join([self.search_name, self.search_query, str(result_count)])

                    time.sleep(1)
                    
                    output_file.write("\t".join([self.search_name, self.search_query, str(result_count)]).encode('utf-8')+"\n")

        print "Processed {0} results".format(total_results)


    def save_docs(self, search_results):
        """Save documents to directory, using dict object to locate docs by search."""

        for doc in search_results["documentContainerList"]["documentContainer"]:


            doc_path = os.path.join(self.home_dir, "output", doc["documentId"]+".xml")

            if not os.path.isfile(doc_path):

                self.search_file_dict.append(
                                        {"search_name":self.search_name,
                                         "search_string":self.search_query,
                                         "document_id":doc["documentId"],

                                        })

                text = base64.b64decode(doc["document"]).decode("utf-8")
                

                with codecs.open(doc_path, "w", "utf-8") as output_file:
                    output_file.write(text)


