import ConfigParser
from wsk_xml import BuildWskXml
from lxml import etree
from suds.client import Client
from suds.transport.http import HttpTransport
import os
import logging
logging.basicConfig(level=logging.ERROR)
#logging.getLogger('suds.client').setLevel(logging.DEBUG)

class WskApi():
    """Use WSK to interact with LexisNexis API."""

    def __init__(self, credentials="default"):
        """
        Get credentials and authenticate.

        Keyword arguments:
        credentials (str) -- codename for credentials which should match section heading in config file.
        """    
        self.credentials = credentials
        self._get_credentials()


    def _get_credentials(self):
        """Use supplied credential codename to access username and password from config file."""

        config_file = os.path.join(os.path.dirname(os.path.abspath(__file__)), "credentials.cfg")
        config = ConfigParser.ConfigParser()
        config.read(config_file)
        self.username = config.get(self.credentials, "username")
        self.password = config.get(self.credentials, "password")
        self.wsdl_file = config.get(self.credentials, "wsdl_file")
        self.authenticate()

    def authenticate(self):

        self.client = Client(self.wsdl_file)
        self.auth_response = self.client.service.Authenticate(authId=self.username, password=self.password)
        self.security_token = self.auth_response["binarySecurityToken"]

    def set_source_information(self, source_ids):
        """
        Set source information object, in order to pass it to the search function.

        Positional arguments:
        source_ids (list) -- list of lexis nexis source IDs to search against.

        Sample source IDs:
        161887 -- News 90 Days, English
        8399 -- News all, English
        232166 -- News 90 Days, Non-English
        148109 -- News all, Non-English
        """

        self.si = self.client.factory.create("ns15:SourceInformationChoice")
        self.si.sourceIdList.sourceId = source_ids

    def set_retrieval_options(self, document_view="FullTextWithTerms", document_markup="Semantic", range_begin="1", range_end="10"):
        """
        Set retrieval options object to define results in search operation.

        Keyword arguments:
        document_view (str) -- one from a list of options for the type of result sought.
        document_markup (str) -- of from a list of options for the type of markup to display.
        range_begin (st) -- document in results to begin with
        range_end (str) -- document in results to end with (document_view variable determines max docs allowed to request)
        """
        self.ro = self.client.factory.create("ns76:RetrievalOptions")
        self.ro.documentView = document_view
        self.ro.documentMarkup = document_markup
        self.ro.documentRange.begin = range_begin
        self.ro.documentRange.end = range_end


    def search(self, search_query):
        """
        Search against sources specified in set_source_information function.

        Positional arguments:
        search_query (str) -- Boolean search query string
        """
        #if self.client["port"] <> "Search":
        self.client.set_options(port="Search")
        self.search_results = self.client.service.Search(binarySecurityToken=self.security_token, sourceInformation=self.si, query=search_query, retrievalOptions=self.ro)
        self.result_count = self.search_results["documentsFound"]


    def _authenticate(self):
        """Use credentials to communicate via WSK and retrieve session token."""
        self._build_xml_root()
        self.soap_auth_ns = "http://authenticate.authentication.services.v1.wsapi.lexisnexis.com"
        self.soap_auth_prefix_value = "{" + self.soap_auth_ns + "}"
        self.auth_namespace = {None: self.soap_auth_ns}
        self.auth_tree = etree.SubElement(self.soap_body, self.ns0_prefix_value + "Authenticate", nsmap=self.auth_namespace)
        etree.SubElement(self.auth_tree, self.ns0_prefix_value + "authId").text = self.username
        etree.SubElement(self.auth_tree, self.ns0_prefix_value + "password").text = self.password
        self.xml_string = etree.tostring(self.root, encoding='utf-8', xml_declaration=True, pretty_print=True)
        self._send_request()


    def _build_xml_root(self):
        """Establish root level element for soap request."""
        self.soap_ns = "http://schemas.xmlsoap.org/soap/envelope/"
        self.xsi = "http://www.w3.org/2001/XMLSchema-instance"
        self.xsi_prefix_value = "{" + self.xsi + "}"
        self.ns0 = "http://authenticate.authentication.services.v1.wsapi.lexisnexis.com"
        self.ns0_prefix_value = "{" + self.ns0 + "}"
        self.ns1 = "http://schemas.xmlsoap.org/soap/envelope/"
        self.ns1_prefix_value = "{" + self.ns1 + "}"
        self.soap_prefix_value = "{" + self.soap_ns + "}"
        self.namespace = {
                          "SOAP-ENV": self.soap_ns,
                          "ns0": self.ns0,
                          "ns1": self.ns1,
                          "xsi": self.xsi,
                          }
        self.root = etree.Element(self.soap_prefix_value + "Envelope", nsmap=self.namespace)
        self.soap_header = etree.SubElement(self.root, self.soap_prefix_value + "Header")
        self.soap_body = etree.SubElement(self.root, self.ns1_prefix_value + "Body")

    def _send_request(self):

        self.client = Client(self.wsdl_file)
        self.auth_response = self.client.service.Authenticate(__inject={'msg': self.xml_string})
        

